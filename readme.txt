git init 						//创建版本库
git add readme.txt  		    //添加readme.txt到缓存区
git add . 						//添加所有文件到暂存区
git commit - m '第一次提交'  	//提交文件到当前分支
git log 					 	//显示从最近到最远的提交日志
git ref 						//记录了每一次提交命令
git reset --hard HEAD^ 			//回退到上一个版本
git resrt --hard 1234 			//回退到1234这个版本,1234代表commit id
cat readme.txt  				//查看文件内容
git checkout --readme.txt       //让这个文件回到最近一次git commit或git add时的状态。
git rm delete.txt   			//删除这个文件,并且commit提交
ssh-keygen -t rsa -C "981017952@qq.com"  //创建SSH Key
git remote add origin git@github.com:michaelliao/learngit.git   //添加关联远程仓库,这是ssh类型,
git push -u oschina master		//使用命令git push -u origin master第一次推送master分支的所有内容,以后不用加-u了
git clone git@github.com:michaelliao/gitskills.git   //克隆远程仓库项目到本地
git checkout -b dev             //创建dev分支，然后切换到dev分支
git branch                      //命令查看当前分支.
git checkout master             //切换回master分支
git merge dev                   //把dev分支的工作成果合并到master分支上
git branch -d dev               //删除dev分支
git stash                       //把当前工作现场“储藏”起来，等以后恢复现场后继续工作
git stash list                  //查看stash保存的内容
git stash pop                  //Git把stash内容存在某个地方，一是用git stash 
								apply恢复，但是恢复后，stash内容并不删除，你需要用git stash drop来删除,另一种方式是用git stash pop，恢复的同时把stash内容也删了：
git branch -D feature			//强行删除分支
git remote rm origin			//删除origin远程仓库
git remote  -v					//查看远程仓库信息
git fetch oschina				//刷新远程仓库信息
git pull                        //最新的提交从origin/dev抓下来
git checkout -b dev origin/dev  //创建远程origin的dev分支到本地,创建本地dev分支
git branch --set-upstream dev origin/dev  //设置dev和origin/dev的链接
git tag <name>                  //就可以打一个新标签
git tag v0.9 6224937            //对add merge这次提交打标签，它对应的commit id是6224937
git tag                         //查看所有标签
git show <tagname>              //查看标签信息
git tag -a v0.1 -m "version 0.1 released" 3628164      //创建带有说明的标签，用-a指定标签名，-m指定说明文字
git push origin <tagname>       //可以推送一个本地标签；
git push origin --tags          //可以推送全部未推送过的本地标签；
git tag -d <tagname>            //可以删除一个本地标签；
git push origin :refs/tags/<tagname>  //可以删除一个远程标签。

团队合作模式:
1.首先，可以试图用git push origin branch-name推送自己的修改；
2.如果推送失败，则因为远程分支比你的本地更新，需要先用git pull试图合并；
3.如果合并有冲突，则解决冲突，并在本地提交；
4.没有冲突或者解决掉冲突后，再用git push origin branch-name推送就能成功！
5.如果git pull提示“no tracking information”，则说明本地分支和远程分支的链接关系没有创建，用命令git branch --set-upstream branch-name origin/branch-name。


1.查看远程库信息，使用git remote -v；
2.本地新建的分支如果不推送到远程，对其他人就是不可见的；
3.从本地推送分支，使用git push origin branch-name，如果推送失败，先用git pull抓取远程的新提交；
4.在本地创建和远程分支对应的分支，使用git checkout -b branch-name origin/branch-name，本地和远程分支的名称最好一致；
5.建立本地分支和远程分支的关联，使用git branch --set-upstream branch-name origin/branch-name；
6.从远程抓取分支，使用git pull，如果有冲突，要先处理冲突。

1.命令git tag <name>用于新建一个标签，默认为HEAD，也可以指定一个commit id；
2.git tag -a <tagname> -m "blablabla..."可以指定标签信息；
3.git tag -s <tagname> -m "blablabla..."可以用PGP签名标签；
4.命令git tag可以查看所有标签。
5.命令git push origin <tagname>可以推送一个本地标签；
6.命令git push origin --tags可以推送全部未推送过的本地标签；
7.命令git tag -d <tagname>可以删除一个本地标签；
8.命令git push origin :refs/tags/<tagname>可以删除一个远程标签。


java官方.gitignore文件

*.class
# Mobile Tools for Java (J2ME)
.mtj.tmp/

# Package Files #
*.jar
*.war
*.ear

# virtual machine crash logs, see http://www.java.com/en/download/help/error_hotspot.xml
hs_err_pid*

教程:
1.添加公钥
2.添加远程仓库  git remote add oschina **** /git remote add coding ****
3.git fetch oschina /coding 
4.git 然后在intellij idea 强行推送分支一次
..